# Eggplant

![Example3 running](screenshots/ss1.png)
![Example3 running](screenshots/ss2.png)

### Installing

1.  Open **Extensions** sidebar panel in VS Code: `View → Extensions`
2.  Search for `Eggplant ` - by **Ahmet zeybek**
3.  Click **Install** to install it.
4.  Click **Reload** to reload the your editor
5.  Code > Preferences > Color Theme > **Eggplant**

## Built With

- [vscode](https://code.visualstudio.com/download) - VS Code editor
- [yo code](https://code.visualstudio.com/docs/extensions/yocode) - Extension Generator
- [vsce](https://code.visualstudio.com/docs/extensions/publish-extension) - Publishing Tool Reference

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details